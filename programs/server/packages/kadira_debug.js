(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var _ = Package.underscore._;
var Random = Package.random.Random;
var check = Package.check.check;
var Match = Package.check.Match;
var MongoInternals = Package.mongo.MongoInternals;
var Mongo = Package.mongo.Mongo;
var Tracker = Package.tracker.Tracker;
var Deps = Package.tracker.Deps;
var Kadira = Package['meteorhacks:kadira'].Kadira;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var Utils, TraceStore, KadiraDebug, AppConfig, SubHandlers;

(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/kadira_debug/lib/utils.js                                                             //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
Utils = Utils || {};                                                                              // 1
                                                                                                  // 2
Utils.getAppEnv = function() {                                                                    // 3
  var env = 'development';                                                                        // 4
  if(!Package['kadira:runtime-dev']) {                                                            // 5
    env = 'production';                                                                           // 6
  }                                                                                               // 7
  return env;                                                                                     // 8
};                                                                                                // 9
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/kadira_debug/lib/server/utils.js                                                      //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
Utils = Utils || {};                                                                              // 1
                                                                                                  // 2
Utils.getDebugAuthKey = function () {                                                             // 3
  var authKey = process.env.KADIRA_DEBUG_AUTH_KEY;                                                // 4
  if(authKey) {                                                                                   // 5
    return authKey;                                                                               // 6
  }                                                                                               // 7
                                                                                                  // 8
  // Getting it from Meteor.settings.                                                             // 9
  authKey = Meteor.settings &&                                                                    // 10
    Meteor.settings.kadira &&                                                                     // 11
    Meteor.settings.kadira.debug &&                                                               // 12
    Meteor.settings.kadira.debug.authKey;                                                         // 13
                                                                                                  // 14
  return authKey;                                                                                 // 15
};                                                                                                // 16
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/kadira_debug/lib/server/trace_store.js                                                //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
var LRUCache = Npm.require('lru-cache');                                                          // 1
                                                                                                  // 2
TraceStore = function() {                                                                         // 3
  this._sessionMapper = {};                                                                       // 4
  this._registeredSessions = {};                                                                  // 5
                                                                                                  // 6
  this._onMethodTrace = this._onMethodTrace.bind(this);                                           // 7
  this._onSubTrace = this._onSubTrace.bind(this);                                                 // 8
                                                                                                  // 9
  this._methodTraces = new LRUCache({max: 1000});                                                 // 10
  this._subTraces = new LRUCache({max: 1000});                                                    // 11
                                                                                                  // 12
  // This is an measurement to make sure, we won't have a                                         // 13
  // memory leak in case of something goes wrong.                                                 // 14
  this._timeEventsCache = new LRUCache({max: 1000});                                              // 15
};                                                                                                // 16
                                                                                                  // 17
TraceStore.prototype.registerSession = function(sessionId, browserId, clientId) {                 // 18
  var key = this._getClientKey(browserId, clientId);                                              // 19
  this._sessionMapper[key] = sessionId;                                                           // 20
  // we store registered sessions in a map                                                        // 21
  // to keep track for futher uses                                                                // 22
  this._registeredSessions[sessionId] = true;                                                     // 23
};                                                                                                // 24
                                                                                                  // 25
TraceStore.prototype.unregisterSession = function(browserId, clientId) {                          // 26
  var key = this._getClientKey(browserId, clientId);                                              // 27
  var sessionId = this._sessionMapper[key];                                                       // 28
  delete this._sessionMapper[key];                                                                // 29
  delete this._registeredSessions[sessionId];                                                     // 30
};                                                                                                // 31
                                                                                                  // 32
TraceStore.prototype.start = function() {                                                         // 33
  // It's okay call `Kadira._startInstrumenting` multiple times                                   // 34
  Kadira._startInstrumenting(function() {});                                                      // 35
                                                                                                  // 36
  Kadira.EventBus.on('method', 'methodCompleted', this._onMethodTrace);                           // 37
  Kadira.EventBus.on('pubsub', 'subCompleted', this._onSubTrace);                                 // 38
};                                                                                                // 39
                                                                                                  // 40
TraceStore.prototype.stop = function() {                                                          // 41
  Kadira.EventBus.removeListener('method', 'methodCompleted', this._onMethodTrace);               // 42
  Kadira.EventBus.removeListener('pubsub', 'subCompleted', this._onSubTrace);                     // 43
};                                                                                                // 44
                                                                                                  // 45
TraceStore.prototype.getTrace = function(browserId, clientId, type, id) {                         // 46
  var key = this._getClientKey(browserId, clientId);                                              // 47
  var sessionId = this._sessionMapper[key];                                                       // 48
  if(!sessionId) {                                                                                // 49
    return;                                                                                       // 50
  }                                                                                               // 51
                                                                                                  // 52
  var traceKey = this._getTraceKey(sessionId, id);                                                // 53
  if(type === "method") {                                                                         // 54
    return this._methodTraces.get(traceKey);                                                      // 55
  } else if(type === "pubsub") {                                                                  // 56
    return this._subTraces.get(traceKey);                                                         // 57
  } else {                                                                                        // 58
    throw new Meteor.Error(400, "Invalid trace type: " + type);                                   // 59
  }                                                                                               // 60
};                                                                                                // 61
                                                                                                  // 62
// Pick all the timevents collection for this client                                              // 63
// Once picked, those data will be removed from the cache                                         // 64
TraceStore.prototype.pickTimeEvents = function(browserId, clientId) {                             // 65
  var key = this._getClientKey(browserId, clientId);                                              // 66
  var sessionId = this._sessionMapper[key];                                                       // 67
                                                                                                  // 68
  if(!this._timeEventsCache.has(sessionId)) {                                                     // 69
    return [];                                                                                    // 70
  }                                                                                               // 71
                                                                                                  // 72
  var cacheItem = this._timeEventsCache.get(sessionId);                                           // 73
  this._timeEventsCache.del(sessionId);                                                           // 74
                                                                                                  // 75
  return cacheItem.times;                                                                         // 76
};                                                                                                // 77
                                                                                                  // 78
/*                                                                                                // 79
  Tracks time related metrics for DDP messages                                                    // 80
  (but possible for others as well)                                                               // 81
                                                                                                  // 82
  @param type - type of the message (pubsub, method)                                              // 83
  @param id - sessionId of the message                                                            // 84
  @param id - id of the message                                                                   // 85
  @param event - event we are tracking the time (eg:- start, end)                                 // 86
  @timestamp [optional] - timestamp of the event in milliseconds                                  // 87
  @info [optional] - an object containing some special information                                // 88
*/                                                                                                // 89
TraceStore.prototype._trackTime = function(type, sessionId, id, event, timestamp, info) {         // 90
  if(typeof timestamp === "object") {                                                             // 91
    info = timestamp;                                                                             // 92
    timestamp = null;                                                                             // 93
  }                                                                                               // 94
                                                                                                  // 95
  timestamp = timestamp || Date.now();                                                            // 96
  if(!this._timeEventsCache.has(sessionId)) {                                                     // 97
    this._timeEventsCache.set(sessionId, {times: []});                                            // 98
  }                                                                                               // 99
                                                                                                  // 100
  var item = {                                                                                    // 101
    type: type,                                                                                   // 102
    id: id,                                                                                       // 103
    event: event,                                                                                 // 104
    timestamp: timestamp                                                                          // 105
  };                                                                                              // 106
                                                                                                  // 107
  if(info) {                                                                                      // 108
    item.info = info;                                                                             // 109
  }                                                                                               // 110
                                                                                                  // 111
  this._timeEventsCache.get(sessionId).times.push(item);                                          // 112
};                                                                                                // 113
                                                                                                  // 114
TraceStore.prototype._onMethodTrace = function(trace, session) {                                  // 115
  if(!this._registeredSessions[session.id]) {                                                     // 116
    // not a valid session,                                                                       // 117
    // return without building a trace                                                            // 118
    return;                                                                                       // 119
  }                                                                                               // 120
                                                                                                  // 121
  // We don't need to track Kadira Debug ping method                                              // 122
  if(trace && trace.name === "kadira.debug.client.updateTimeline") {                              // 123
    return;                                                                                       // 124
  }                                                                                               // 125
  this._buildTrace(trace);                                                                        // 126
                                                                                                  // 127
  var key = this._getTraceKey(session.id, trace.id);                                              // 128
  this._methodTraces.set(key, trace);                                                             // 129
  this._trackTraceTimes('method', session.id, trace);                                             // 130
};                                                                                                // 131
                                                                                                  // 132
TraceStore.prototype._onSubTrace = function(trace, session) {                                     // 133
  if(!this._registeredSessions[session.id]) {                                                     // 134
    // not a valid session,                                                                       // 135
    // return without building a trace                                                            // 136
    return;                                                                                       // 137
  }                                                                                               // 138
                                                                                                  // 139
  // here, trace can be empty                                                                     // 140
  if(trace) {                                                                                     // 141
    this._buildTrace(trace);                                                                      // 142
    var key = this._getTraceKey(session.id, trace.id);                                            // 143
    this._subTraces.set(key, trace);                                                              // 144
    this._trackTraceTimes('pubsub', session.id, trace);                                           // 145
  }                                                                                               // 146
};                                                                                                // 147
                                                                                                  // 148
TraceStore.prototype._getTraceKey = function(session, traceId) {                                  // 149
  return session + traceId;                                                                       // 150
};                                                                                                // 151
                                                                                                  // 152
TraceStore.prototype._getClientKey = function(browserId, clientId) {                              // 153
  return browserId + clientId;                                                                    // 154
};                                                                                                // 155
                                                                                                  // 156
// We need alter the tracer to make it compatible with the format                                 // 157
// tracer viewer accepts.                                                                         // 158
TraceStore.prototype._buildTrace = function(trace) {                                              // 159
  trace.startTime = new Date(trace.at);                                                           // 160
  if(trace && trace.metrics && trace.metrics.total) {                                             // 161
    trace.totalValue = trace.metrics.total;                                                       // 162
  }                                                                                               // 163
                                                                                                  // 164
  return trace;                                                                                   // 165
};                                                                                                // 166
                                                                                                  // 167
TraceStore.prototype._trackTraceTimes = function(type, sessionId, trace) {                        // 168
  var info = {name: trace.name};                                                                  // 169
  this._trackTime(type, sessionId, trace.id, 'server-received', trace.at, info);                  // 170
  this._trackTime(type, sessionId, trace.id, 'server-waitend', trace.at + trace.metrics.wait);    // 171
  this._trackTime(type, sessionId, trace.id, 'server-processed', trace.at + trace.metrics.total);
};                                                                                                // 173
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/kadira_debug/lib/server/connect.js                                                    //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
KadiraDebug = KadiraDebug || {};                                                                  // 1
var LRUCache = Npm.require('lru-cache');                                                          // 2
var UAParser = Npm.require('ua-parser-js');                                                       // 3
                                                                                                  // 4
AppConfig = {                                                                                     // 5
  // app environment. Either `production` or `development`.                                       // 6
  env: Utils.getAppEnv(),                                                                         // 7
                                                                                                  // 8
  // auth key taken from enviornment variables                                                    // 9
  authKey: Utils.getDebugAuthKey(),                                                               // 10
                                                                                                  // 11
  // This is a set of token passed from the client, these token will be used to                   // 12
  // authorize a browser session in the production mode.                                          // 13
  // It's possible for a single accessToken to be used in multiple times.                         // 14
  accessTokens: new LRUCache({max: 1000}),                                                        // 15
                                                                                                  // 16
  // These maps hold authorized Kadira Debug DDP sessionIds                                       // 17
  authorizedSessions: {                                                                           // 18
    // sessions made from the app's client                                                        // 19
    client: {},                                                                                   // 20
    // sessions made from the Kadira Debug UI                                                     // 21
    remote: {}                                                                                    // 22
  }                                                                                               // 23
};                                                                                                // 24
                                                                                                  // 25
var traceStore = new TraceStore();                                                                // 26
traceStore.start();                                                                               // 27
                                                                                                  // 28
SubHandlers = {                                                                                   // 29
  // Keep DDP sessions of the remote admin's subscription for timeline                            // 30
  timeline: [],                                                                                   // 31
  // Keep DDP sessions of the client app's subscription for sending the                           // 32
  // count of remote admins                                                                       // 33
  listeners: []                                                                                   // 34
};                                                                                                // 35
var uniqueId = 0;                                                                                 // 36
var serverId = Random.id();                                                                       // 37
                                                                                                  // 38
// publications                                                                                   // 39
                                                                                                  // 40
Meteor.publish('kadira.debug.remote.auth', function(authKey) {                                    // 41
  check(authKey, Match.Any);                                                                      // 42
                                                                                                  // 43
  // We need to check authentication of a production app only.                                    // 44
  // For other development apps, we can use KD without any authentication                         // 45
  if(AppConfig.env === 'production') {                                                            // 46
    if(authKey !== AppConfig.authKey) {                                                           // 47
      throw new Meteor.Error('401', 'Unauthorized.');                                             // 48
    }                                                                                             // 49
                                                                                                  // 50
    var kadiraInfo = Kadira._getInfo();                                                           // 51
    var sessionId = kadiraInfo.session;                                                           // 52
    AppConfig.authorizedSessions.remote[sessionId] = true;                                        // 53
  }                                                                                               // 54
                                                                                                  // 55
  this.onStop(function() {                                                                        // 56
    delete AppConfig.authorizedSessions.remote[sessionId];                                        // 57
  });                                                                                             // 58
                                                                                                  // 59
  this.ready();                                                                                   // 60
});                                                                                               // 61
                                                                                                  // 62
Meteor.publish('kadira.debug.remote.timeline', function() {                                       // 63
  KadiraDebug._authorize('remote');                                                               // 64
                                                                                                  // 65
  this.ready();                                                                                   // 66
  var sub = this;                                                                                 // 67
                                                                                                  // 68
  SubHandlers.timeline.push(sub);                                                                 // 69
  updateListenersCount();                                                                         // 70
                                                                                                  // 71
  sub.onStop(function() {                                                                         // 72
    var index = SubHandlers.timeline.indexOf(sub);                                                // 73
    SubHandlers.timeline.splice(index, 1);                                                        // 74
    updateListenersCount();                                                                       // 75
  });                                                                                             // 76
});                                                                                               // 77
                                                                                                  // 78
Meteor.publish('kadira.debug.client.auth', function(accessToken) {                                // 79
  check(accessToken, Match.Any);                                                                  // 80
                                                                                                  // 81
  // We need to check the accessToken in the production mode only.                                // 82
  // On development mode we don't need these checks.                                              // 83
  if(AppConfig.env === 'production') {                                                            // 84
    if(!AppConfig.accessTokens.has(accessToken)) {                                                // 85
      throw new Meteor.Error('401', 'Unauthorized.');                                             // 86
    }                                                                                             // 87
                                                                                                  // 88
    var kadiraInfo = Kadira._getInfo();                                                           // 89
    var sessionId = kadiraInfo.session;                                                           // 90
                                                                                                  // 91
    AppConfig.authorizedSessions.client[sessionId] = true;                                        // 92
  }                                                                                               // 93
                                                                                                  // 94
  this.onStop(function() {                                                                        // 95
    delete AppConfig.authorizedSessions.client[sessionId];                                        // 96
  });                                                                                             // 97
                                                                                                  // 98
  this.ready();                                                                                   // 99
});                                                                                               // 100
                                                                                                  // 101
Meteor.publish('kadira.debug.client.listeners', function() {                                      // 102
  KadiraDebug._authorize('client');                                                               // 103
                                                                                                  // 104
  var sub = this;                                                                                 // 105
  var timelineCount = SubHandlers.timeline.length;                                                // 106
                                                                                                  // 107
  sub.added('kdInfo', 'listeners-count', {count: timelineCount});                                 // 108
  sub.ready();                                                                                    // 109
                                                                                                  // 110
  SubHandlers.listeners.push(sub);                                                                // 111
  sub.onStop(function() {                                                                         // 112
    var index = SubHandlers.listeners.indexOf(sub);                                               // 113
    SubHandlers.listeners.splice(index, 1);                                                       // 114
  });                                                                                             // 115
});                                                                                               // 116
                                                                                                  // 117
Meteor.publish('kadira.debug.client.init', function(browserId, clientId) {                        // 118
  check(browserId, String);                                                                       // 119
  check(clientId, String);                                                                        // 120
                                                                                                  // 121
  KadiraDebug._authorize('client');                                                               // 122
                                                                                                  // 123
  var kadiraInfo = Kadira._getInfo();                                                             // 124
  if(kadiraInfo) {                                                                                // 125
    var sessionId = kadiraInfo.session;                                                           // 126
                                                                                                  // 127
    traceStore.registerSession(sessionId, browserId, clientId);                                   // 128
                                                                                                  // 129
    this.onStop(function() {                                                                      // 130
      traceStore.unregisterSession(browserId, clientId);                                          // 131
    });                                                                                           // 132
  }                                                                                               // 133
                                                                                                  // 134
  this.ready();                                                                                   // 135
});                                                                                               // 136
                                                                                                  // 137
// methods                                                                                        // 138
Meteor.methods({                                                                                  // 139
  'kadira.debug.client.updateTimeline': function(browserId, clientId, data) {                     // 140
    check(browserId, String);                                                                     // 141
    check(clientId, String);                                                                      // 142
    check(data, Object);                                                                          // 143
                                                                                                  // 144
    KadiraDebug._authorize('client');                                                             // 145
                                                                                                  // 146
    // Pick tracked server time events and push them to the                                       // 147
    // client's payload                                                                           // 148
    // So, we can send them back to the server                                                    // 149
    var serverTimeEvents = traceStore.pickTimeEvents(browserId, clientId);                        // 150
    serverTimeEvents.forEach(function(ev) {                                                       // 151
      data.times.push(ev);                                                                        // 152
    });                                                                                           // 153
                                                                                                  // 154
    // set unique id for each server sessions                                                     // 155
    data.serverId = serverId;                                                                     // 156
                                                                                                  // 157
    // update timeline                                                                            // 158
    // XXX: Try to send these to timeline which monitor the given access token                    // 159
    SubHandlers.timeline.forEach(function(sub) {                                                  // 160
      var id = 'id' + ++uniqueId;                                                                 // 161
      sub.added('kdTimeline', id, {                                                               // 162
        browserId: browserId,                                                                     // 163
        clientId: clientId,                                                                       // 164
        data: data                                                                                // 165
      });                                                                                         // 166
      sub.removed('kdTimeline', id);                                                              // 167
    });                                                                                           // 168
  },                                                                                              // 169
                                                                                                  // 170
  'kadira.debug.client.getBrowserName': function(userAgent) {                                     // 171
    check(userAgent, String);                                                                     // 172
                                                                                                  // 173
    KadiraDebug._authorize('client');                                                             // 174
                                                                                                  // 175
    var parser = new UAParser(userAgent);                                                         // 176
    return parser.getResult().browser.name;                                                       // 177
  },                                                                                              // 178
                                                                                                  // 179
  'kadira.debug.remote.getTrace': function(browserId, clientId, type, id) {                       // 180
    check(browserId, String);                                                                     // 181
    check(clientId, String);                                                                      // 182
    check(type, String);                                                                          // 183
    check(id, String);                                                                            // 184
                                                                                                  // 185
    KadiraDebug._authorize('remote');                                                             // 186
                                                                                                  // 187
    return traceStore.getTrace(browserId, clientId, type, id);                                    // 188
  },                                                                                              // 189
                                                                                                  // 190
  'kadira.debug.remote.getAppEnv': function() {                                                   // 191
    return AppConfig.env;                                                                         // 192
  },                                                                                              // 193
                                                                                                  // 194
  'kadira.debug.remote.createAccessToken': function() {                                           // 195
    KadiraDebug._authorize('remote');                                                             // 196
                                                                                                  // 197
    var kadiraInfo = Kadira._getInfo();                                                           // 198
                                                                                                  // 199
    var accessToken = Random.id(4);                                                               // 200
    AppConfig.accessTokens.set(accessToken, kadiraInfo.session);                                  // 201
                                                                                                  // 202
    return accessToken;                                                                           // 203
  },                                                                                              // 204
                                                                                                  // 205
  'kadira.debug.remote.reset': function() {                                                       // 206
    KadiraDebug._authorize('remote');                                                             // 207
                                                                                                  // 208
    AppConfig.accessTokens.reset();                                                               // 209
    AppConfig.authorizedSessions = {                                                              // 210
      client: {},                                                                                 // 211
      remote: {}                                                                                  // 212
    };                                                                                            // 213
  }                                                                                               // 214
});                                                                                               // 215
                                                                                                  // 216
function updateListenersCount() {                                                                 // 217
  SubHandlers.listeners.forEach(function(sub) {                                                   // 218
    var timelineCount = SubHandlers.timeline.length;                                              // 219
    sub.changed('kdInfo', 'listeners-count', {count: timelineCount});                             // 220
  });                                                                                             // 221
}                                                                                                 // 222
                                                                                                  // 223
KadiraDebug._authorize = function(type, sessionId) {                                              // 224
  if(AppConfig.env === 'development') {                                                           // 225
    return true;                                                                                  // 226
  }                                                                                               // 227
                                                                                                  // 228
  if(!sessionId) {                                                                                // 229
    var kadiraInfo = Kadira._getInfo();                                                           // 230
    var sessionId = kadiraInfo.session;                                                           // 231
  }                                                                                               // 232
                                                                                                  // 233
  if(AppConfig.authorizedSessions[type][sessionId]) {                                             // 234
    return true;                                                                                  // 235
  } else {                                                                                        // 236
    throw new Meteor.Error('401', 'Unauthorized.');                                               // 237
  }                                                                                               // 238
};                                                                                                // 239
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['kadira:debug'] = {
  KadiraDebug: KadiraDebug
};

})();

//# sourceMappingURL=kadira_debug.js.map
