(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// wp.js                                                               //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
Pairs = new Mongo.Collection("pairs");                                 // 1
                                                                       //
// ------  Server  ------                                              //
                                                                       //
if (Meteor.isServer) {                                                 // 6
                                                                       //
    Meteor.publish("pairs", function () {                              // 9
        return Pairs.find({                                            // 10
            owner: this.userId                                         // 11
        });                                                            //
    });                                                                //
}                                                                      //
                                                                       //
// ------  Client  ------                                              //
                                                                       //
if (Meteor.isClient) {                                                 // 19
                                                                       //
    // startup                                                         //
    Meteor.startup(function () {                                       // 23
        if (Session.equals("glStep", undefined)) {                     // 24
            Session.set({                                              // 25
                glStep: 0,                                             // 26
                marked: [],                                            // 27
                deleted: [],                                           // 28
                hideOne: false                                         // 29
            });                                                        //
        }                                                              //
    });                                                                //
                                                                       //
    // subscribe                                                       //
    Meteor.subscribe('pairs');                                         // 36
                                                                       //
    // ----  Body  ----                                                //
                                                                       //
    Template.body.helpers({                                            // 41
                                                                       //
        pairs: function () {                                           // 43
            cur_step = Session.get('glStep');                          // 44
            id_list = Session.get("cur_step");                         // 45
            console.log("cur_step: " + cur_step);                      // 46
            console.log("id_list: " + id_list);                        // 47
            if (cur_step === 0) {                                      // 48
                return Pairs.find();                                   // 49
            } else {                                                   //
                return Pairs.find({                                    // 52
                    _id: { $in: id_list }                              // 53
                });                                                    //
            }                                                          //
        },                                                             //
                                                                       //
        hideOne: function () {                                         // 58
            return Session.get("hideOne");                             // 59
        },                                                             //
                                                                       //
        glStep: function () {                                          // 62
            return Session.get("glStep");                              // 63
        }                                                              //
    });                                                                //
                                                                       //
    Template.body.events({                                             // 67
                                                                       //
        "click #check": function () {                                  // 69
            console.log('check button');                               // 70
                                                                       //
            hideOne = Session.get("hideOne");                          // 72
            Session.set("hideOne", !hideOne);                          // 73
        },                                                             //
                                                                       //
        "click #step": function () {                                   // 76
            console.log('step button');                                // 77
                                                                       //
            cur_step = Session.get("glStep");                          // 79
                                                                       //
            marked = Session.get('marked');                            // 81
            Session.set('cur_step', marked);                           // 82
            Session.set('marked', []);                                 // 83
            Session.set({ glStep: cur_step + 1 });                     // 84
                                                                       //
            //  delete elements                                        //
            deleted = Session.get('deleted');                          // 87
            Meteor.call("deletePairs", deleted);                       // 88
                                                                       //
            if (Session.equals("hideOne", true)) {                     // 90
                document.getElementById('check').click();              // 91
            }                                                          //
        },                                                             //
                                                                       //
        "click #clear": function () {                                  // 95
            console.log('clear button');                               // 96
                                                                       //
            Session.set("glStep", 0);                                  // 98
        },                                                             //
                                                                       //
        "submit .new_pair": function (event) {                         // 101
            console.log('new_pair button');                            // 102
                                                                       //
            event.preventDefault();                                    // 104
            var text = event.target.text.value;                        // 105
            Meteor.call("addPair", text);                              // 106
            event.target.text.value = "";                              // 107
        },                                                             //
                                                                       //
        "change .file-upload input": function (event) {                // 110
            console.log('file-upload input');                          // 111
                                                                       //
            var file = event.target.files[0];                          // 113
            var reader = new FileReader();                             // 114
            reader.onload = function (fileLoadEvent) {                 // 115
                Meteor.call('file-upload', file, reader.result);       // 116
            };                                                         //
            reader.readAsText(file);                                   // 118
        }                                                              //
    });                                                                //
                                                                       //
    // ----  Pair  ----                                                //
                                                                       //
    Template.pair.helpers({                                            // 126
                                                                       //
        hideOne: function () {                                         // 128
            return Session.get("hideOne");                             // 129
        },                                                             //
                                                                       //
        isMarked: function (pair) {                                    // 132
            all_marked = Session.get('marked');                        // 133
            return all_marked.indexOf(this._id) === -1 ? false : true;
        },                                                             //
                                                                       //
        isDeleted: function (pair) {                                   // 137
            all_deleted = Session.get('deleted');                      // 138
            return all_deleted.indexOf(this._id) === -1 ? false : true;
        }                                                              //
    });                                                                //
                                                                       //
    Template.pair.events({                                             // 143
                                                                       //
        "click .toggle-marked": function () {                          // 145
            console.log("toggle-marked button");                       // 146
                                                                       //
            all_marked = Session.get('marked');                        // 148
            all_deleted = Session.get('deleted');                      // 149
                                                                       //
            index = all_marked.indexOf(this._id);                      // 151
            del_index = all_deleted.indexOf(this._id);                 // 152
                                                                       //
            if (index === -1) {                                        // 154
                                                                       //
                if (del_index !== -1) {                                // 156
                    // if was deleted, undo                            //
                    all_deleted.splice(del_index, 1);                  // 157
                    Session.set('deleted', all_deleted);               // 158
                }                                                      //
                                                                       //
                all_marked.push(this._id);                             // 161
            } else {                                                   //
                all_marked.splice(index, 1);                           // 163
            }                                                          //
            Session.set('marked', all_marked);                         // 165
        },                                                             //
                                                                       //
        "click .toggle-deleted": function () {                         // 168
            console.log("toggle-deleted button");                      // 169
                                                                       //
            all_marked = Session.get('marked');                        // 171
            all_deleted = Session.get('deleted');                      // 172
                                                                       //
            index = all_marked.indexOf(this._id);                      // 174
            del_index = all_deleted.indexOf(this._id);                 // 175
                                                                       //
            if (del_index === -1) {                                    // 177
                                                                       //
                if (index !== -1) {                                    // 179
                    // if was marked, deleted marked                   //
                    all_marked.splice(index, 1);                       // 180
                    Session.set('marked', all_marked);                 // 181
                }                                                      //
                                                                       //
                all_deleted.push(this._id);                            // 184
            } else if (del_index !== -1) {                             //
                all_deleted.splice(del_index, 1);                      // 187
            }                                                          //
                                                                       //
            Session.set('deleted', all_deleted);                       // 190
        }                                                              //
                                                                       //
    });                                                                //
                                                                       //
    // ---- Accounts ----                                              //
                                                                       //
    Accounts.ui.config({                                               // 198
                                                                       //
        passwordSignupFields: "USERNAME_ONLY"                          // 200
                                                                       //
    });                                                                //
}                                                                      //
                                                                       //
// ------  Methods ------                                              //
                                                                       //
Meteor.methods({                                                       // 208
                                                                       //
    addPair: function (text) {                                         // 210
                                                                       //
        if (!Meteor.userId()) {                                        // 212
            throw new Meteor.Error("not-authorized");                  // 213
        }                                                              //
                                                                       //
        // assumed it is "word word" string                            //
        var words = text.split(/ /);                                   // 217
        Pairs.insert({                                                 // 218
            word_0: words[0],                                          // 219
            word_1: words[1],                                          // 220
            owner: Meteor.userId(),                                    // 221
            username: Meteor.user().username,                          // 222
            no_meaning: false                                          // 223
        });                                                            //
    },                                                                 //
                                                                       //
    deletePairs: function (pairIds) {                                  // 227
                                                                       //
        Pairs.remove({                                                 // 229
            owner: this.userId,                                        // 230
            _id: { $in: pairIds }                                      // 231
        });                                                            //
    },                                                                 //
                                                                       //
    'file-upload': function (fileInfo, fileData) {                     // 235
                                                                       //
        if (!Meteor.userId()) {                                        // 237
            throw new Meteor.Error("not-authorized");                  // 238
        }                                                              //
                                                                       //
        var pairs = fileData.split(/\n/);                              // 241
        for (var i = 0; i < pairs.length; i++) {                       // 242
            Meteor.call("addPair", pairs[i]);                          // 243
        }                                                              //
    }                                                                  //
});                                                                    //
/////////////////////////////////////////////////////////////////////////

}).call(this);

//# sourceMappingURL=wp.js.map
